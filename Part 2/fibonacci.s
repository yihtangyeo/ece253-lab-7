	.text
	.global _start
	
_start:
	
	addi r3, r0, 10					/* Fibonacci series n=10 */
	movia sp, 0x0005000           	/* initialize stack pointer to points at the bottom of memory */
	
	subi sp, sp, 4
	stw r3, (sp)
	
	call FIBONACCI
	
	
	END:
	br END
	
FIBONACCI:
		
	/* reset r2 = 0. Note that in the case where r3 > 0, there is no need to send back a 1, the only situation is when you reach the end of FB(1) = 1.
		Else, if the value of r3 is larger than 1, then you merely need to call the RECURSE subroutine to reduce it by 1, you don't need to send a 1 
		to r2 and carry it to the RECURSE subroutine */
	mov r2, r0						
	addi r4, r0, 1					/* use r4 to store 1 to check end condition */
	
	ldw r3, (sp)
	addi sp, sp, 4
	
	beq r3, r0, RETURN_ZERO			/* check if zero, return 0 */
	beq r3, r4, RETURN_ONE			/* check if one, return 1 */
	br RECURSE						/* if none of both, call recursive fibonacci */
	
	RETURN_ZERO:
		add r2, r0, r0
		br END_FIB
	
	RETURN_ONE:
		addi r2, r0, 1
		br END_FIB
	
	END_FIB:
		ret
	
RECURSE:
	subi sp, sp, 12						/* allocate more memory on the stack to store history */
	stw r3, (sp)						
	stw r2, 4(sp)						
	stw ra, 8(sp)
	
	subi r3, r3, 1						/* subtract 1 from r3 */
	subi sp, sp, 4
	stw r3, (sp)
	call FIBONACCI						/* this subroutine will keep get called until r3 has a value of 1 or 0 */
	
	ldw r5, 4(sp)						/* recall that we store old temporary value of the output at sp+4, now recall it */
	add r2, r2, r5						/* add with the current r2 read at final condition */
	stw r2, 4(sp)						/* store it back to stack memory */
	
	/* beyond this part of the code, the FIBONACCI has finished doing everything that recursively goes from r3 - 1.
		Now we need to consider r3 - 2 case now and add them to r2, the output of this recursive subroutine */
	
	ldw r3, (sp)						/* load the original r3 from stack because the value has been altered when we subi r3, 1 */
	stw r3, (sp)						/* store it back to stack memory */	
	
	subi r3, r3, 2						/* subtract 2 from r3 */
	subi sp, sp, 4
	stw r3, (sp)
	call FIBONACCI						/* this subroutine will keep get called until r3 has a value of 1 or 0 */
	
	ldw r5, 4(sp)						/* similar to what was done previously, recall the stored temp value of output */
	add r2, r2, r5						/* add with the current r2 read at final condition */
	
	ldw ra, 8(sp)						/* reload back from stack */
	ldw r3, (sp)
	addi sp, sp, 12
	
	ret

	.end