/* equivalent C code
	
	for (i = MAX_LENGTH; i > 1; i--){
		for (j = 0; j < i-1; j++){
			if (LIST[j] < LIST[j+1]){
				temporary = LIST[j];
				LIST[j] = LIST[j+1];
				LIST[j+1] = temporary;
			}
		}
	}

*/


	.text
	.global _start
	
_start:
	
	movia r13, LIST_WORDS
	movia sp, 0x007ffffc           		/* initialize stack pointer to points at the bottom of memory */
	ldw r12, 0(r13)						/* load length of LIST_WORDS into register r12, which is 10 */
	
	
	LOOP_LENGTH:						/* outer loop that decreases the length to sort at every iteration */
		addi r7, r0, 1
		beq r12, r7, EXITLOOP_LENGTH		/* if it's one, stop looping */		
		addi r4, r13, 4					/* reset address, start from the beginning but skipping the length word */	
		mov r3, r0						/* load r3 with 0 as an inner smaller loop, reset before enter loop */
		
		LOOP_CHAR:						/* inner loop that compares number side to side */
			
			subi r6, r12, 2				/* r6 is to indicate that we want the loop to execute when r3 <= r12 -2 */
			beq r3, r6, EXITLOOP_CHAR	/* if length exceeds, jump out of this loop and go to a higher hierary one */
			
			call SWAP
			addi r4, r4, 4				/* before end of loop, add r4 by 4 to go back to the next word */			
			addi r3, r3, 1				/* add one to the counter */
		br LOOP_CHAR					/* loop back to LOOP_CHAR */
		
		EXITLOOP_CHAR:		
			subi r12, r12, 1				/* subtract one at the end */
			
	br LOOP_LENGTH						/* go back to LOOP_LENGTH */
		
	EXITLOOP_LENGTH:
		br EXITLOOP_LENGTH				/* stay here when done */
	
	SWAP:								/* INPUT: r4, OUTPUT: r2 */
		subi sp, sp, 8
		stw r14, 0(sp)
		stw r15, 4(sp)
		
		ldw r14, (r4)					/* take in the first letter */	
		ldw r15, 4(r4)					/* take in the second letter */		
		mov r2, r0						/* reset r2 = 0 when no swapping occurs */
			
		bgeu r14, r15, IF_NO_SWAP		/* if r14 >= r15, don't sort */
			stw r14, 4(r4)				/* SWAP: r14 will take r15 value, which is to store value of r14 in r15 address */
			stw r15, (r4)				/* SWAP: r15 will take r14 value, which is to store value of r15 in r14 address */
			addi r2, r0, 1				/* SWAP performed: r2 = 1 */
		
		IF_NO_SWAP:
		ldw r14, 0(sp)
		ldw r15, 4(sp)
		addi sp, sp, 8
		ret


	.data
LIST_WORDS:
	.word 	10, 1400, 45, 23, 5, 3, 8, 17, 4, 20, 33
	.end