	.text
	.global _start
	
_start:
	
	movia r16, 0x10000000			/* LEDR base address */
	movia r17, 0x10000050			/* KEY base address */
	ldw r15, PATTERN_BACK_END(r0)	/* load pattern from memory*/
	
	/* set up delay timer */
	movia r18, 0x10002000			/* status register */
	ldw r12, DELAY_VALUE(r0)		/* get data value from memory */
	mov r20, r12					/* store r12 into r20 as backup */
	sthio r12, 8(r18)				/* store half word in low counter */
	srli r12, r12, 16				/* lofical shift right by 16 */
	sthio r12, 12(r18)				/* store half word in high counter */
	movi r19, 0b0110				/* control register: set START=1 and CONT=1 */
	sthio r19, 4(r18)				/* write into control register */
	
	addi r2, r0, 1					/* r2 is either 0 or 1: 0 shift right, 1 shift left, by default, shift left */
	addi r5, r0, 1					/* r5 is either 0 or 1: 0 light stop rotation, 1: light keeping rotation */
	
	TURN_ON_THE_LIGHTS:
		stwio r15, 0(r16)			/* show pattern on LEDR */
		
		
		CHECK_FOR_KEY1:
			ldwio r8, (r17)
			andi r8, r8, 0b0010			/* check if KEY1 is pressed */
			beq r8, r0, CHECK_FOR_KEY2
			mov r12, r20					/* restore r12 from r20 backup */
			srli r12, r12, 1
			mov r20, r12					/* backup it again for future use */
			sthio r12, 8(r18)				/* store half word in low counter */
			srli r12, r12, 16				/* lofical shift right by 16 */
			sthio r12, 12(r18)				/* store half word in high counter */
			movi r19, 0b0110				/* control register: set START=1 and CONT=1 */
			sthio r19, 4(r18)				/* write into control register */
		WAIT_KEY1:
			ldwio r8, (r17)				/* check if KEY is pressed */
			andi r8, r8, 0b0010			/* mask to read for KEY3 only */
			bne r8, r0, WAIT_KEY1		
		
		CHECK_FOR_KEY2:
			ldwio r8, (r17)
			andi r8, r8, 0b0100			/* check if KEY2 is pressed */
			beq r8, r0, CHECK_FOR_KEY3
			mov r12, r20					/* restore r12 from r20 backup */
			slli r12, r12, 1
			mov r20, r12					/* backup it again for future use */
			sthio r12, 8(r18)				/* store half word in low counter */
			srli r12, r12, 16				/* lofical shift right by 16 */
			sthio r12, 12(r18)				/* store half word in high counter */
			movi r19, 0b0110				/* control register: set START=1 and CONT=1 */
			sthio r19, 4(r18)				/* write into control register */
		WAIT_KEY2:
			ldwio r8, (r17)				/* check if KEY is pressed */
			andi r8, r8, 0b0100			/* mask to read for KEY3 only */
			bne r8, r0, WAIT_KEY2
			
		CHECK_FOR_KEY3:
			ldwio r8, (r17)				/* check if KEY is pressed */
			andi r8, r8, 0b1000			/* mask to read for KEY3 only */
			beq r8, r0, DELAY			
		mov r5, r0 						/* kill the rotation */
		WAIT_KEY3:
			ldwio r8, (r17)				/* check if KEY is pressed */
			andi r8, r8, 0b1000			/* mask to read for KEY3 only */
			bne r8, r0, WAIT_KEY3
		CHECK_FOR_KEY3_LIGHTOFF:
			ldwio r8, (r17)				/* check if KEY is pressed */
			andi r8, r8, 0b1000			/* mask to read for KEY3 only */
			beq r8, r0, CHECK_FOR_KEY3_LIGHTOFF			
		addi r5, r0, 1 						/* add the rotation */
		WAIT_KEY3_LIGHTOFF:
			ldwio r8, (r17)				/* check if KEY is pressed */
			andi r8, r8, 0b1000			/* mask to read for KEY3 only */
			bne r8, r0, WAIT_KEY3_LIGHTOFF	
		
		DELAY:
			ldwio r7, (r18)			/* read status register */
			andi r7, r7, 1			/* mask all other bit except the TO bit at lst LSB position */
			beq r7, r0, DELAY		/* if it's not 1 yet, keep polling */
			stwio r7, (r18)			/* if it's 1 already, write r7 into status register to clear TO bit */
		
		beq r2, r0, SHIFT_RIGHT 	/* if r2 = 0, indicates shift right */
		
		SHIFT_LEFT:
			slli r15, r15, 1		/* shift left, but need to take care if it's the reaches the leftmost position */
			ldw r3, PATTERN_FRONT_END(r0)
			bne r15, r3, CONTINUE_SHIFT_LEFT
			mov r2, r0				/* if reach the left most, reverse direction to right by setting r2 to 0 */
			CONTINUE_SHIFT_LEFT:
			br TURN_ON_THE_LIGHTS
			
		SHIFT_RIGHT:
			srli r15, r15, 1		/* shift right, but need to take care if it's the reaches the rightmost position */
			ldw r4, PATTERN_BACK_END(r0)
			bne r15, r4, CONTINUE_SHIFT_RIGHT
			addi r2, r0, 1			/* if reach the right most, reverse direction to right by setting r2 to 1 */
			CONTINUE_SHIFT_RIGHT:
			br TURN_ON_THE_LIGHTS
	
	.data
PATTERN_BACK_END:
	.word 0x00000001
PATTERN_FRONT_END:
	.word 0x00020000
DELAY_VALUE:
	.word 0x200000
	.end