/********************************************************************************
 * This program demonstrates use of parallel ports in the DE2 Basic Computer
 *
 * It performs the following: 
 * 	1. displays the SW switch values on the red LEDR
 * 	2. displays a rotating pattern on the HEX displays
 * 	3. if KEY[3..1] is pressed, uses the SW switches as the pattern
********************************************************************************/
	.text										/* executable code follows */
	.global	_start
_start:

	/* initialize base addresses of parallel ports */
	movia		r16, 0x10000000			/* red LED base address */
	movia		r20, 0x10000020			/* HEX3_HEX0 base address */
	movia		r15, 0x10000040			/* SW slider switch base address */
	movia		r17, 0x10000050			/* pushbutton KEY base address */
	movia		r19, HEX_bits
	ldw		r6, 0(r19)					/* load pattern for HEX displays */

	movia		r18, 0x10002000			/* internal timer base address */
	/* set the interval timer period for scrolling the HEX displays */
	ldw		r12, DELAY_VALUE(r0)		/* 1/(50 MHz) x (0x190000) = 33 msec */
	sthio		r12, 8(r18)					/* store low half word of counter start value */ 
	srli		r12, r12, 16
	sthio		r12, 0xC(r18)				/* high half word of counter start value */ 

	/* start interval timer */
	movi		r19, 0b0110					/* START = 1, CONT = 1 */
	sthio		r19, 4(r18)

DO_DISPLAY:
	ldwio		r4, 0(r15)					/* load slider switches */
	stwio		r4, 0(r16)					/* write to red LEDs */

	ldwio		r5, 0(r17)					/* load pushbuttons */
	beq		r5, r0, NO_BUTTON	
	mov		r6, r4						/* use SW switch values for HEX displays */
WAIT:
	ldwio		r5, 0(r17)					/* load pushbuttons */
	bne		r5, r0, WAIT				/* wait for button release */

NO_BUTTON:
	stwio		r6, 0(r20)					/* store to HEX3 ... HEX0 */
	roli		r6, r6, 1					/* rotate the displayed pattern */

DELAY:										/* wait for the timer to expire */

	ldw		r7, (r18)
	andi		r7, r7, 1
	beq		r7, r0, DELAY	
	stw		r7, (r18)					/* reset T0 bit, keep timer running */

	br 		DO_DISPLAY

	.data										/* data follows */
HEX_bits:
	.word 0x0000000F
DELAY_VALUE:
	.word 0x190000
