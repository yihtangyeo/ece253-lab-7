/* Recursively find the sum of numbers from 0 to N */
/* Equivalent C code recursive function would be:
		int FINDSUM(int N)
		{
			if (N == 0)
				return 0;
			else 
				return N + FINDSUM(N-1);
		}
*/
	.text
	.global _start
_start:
	movia 	sp, 0x20000					/* stack starts from largest memory address */
	add		r2, r0, r0					/* clear r2, where the result ends up */

	movia		r8, DATA_ADDR 				/* Load r8 with the address of the data */

	ldw  		r4, (r8) 					/* Load the data into r4, for subroutine */
	call		FINDSUM						/* Result will be in r2 */

END:
	br 		END  							/* Wait here */

/* Recursively add the numbers from 1 to the value in register r4
 * Input is in r4
 * Result returned in r2
*/
FINDSUM:
	bne		r4, r0, RECURSE			/* if r4 is not 0, then recurse */
	add		r2, r0, r0					/* else, just return 0 */
	ret

RECURSE:
	subi		sp, sp, 8					/* reserve space on the stack */
   stw		r4, 0(sp)					/* save r4 */
   stw		ra, 4(sp)					/* save return address */

	subi		r4, r4, 1					/* else, recurse on n-1 */
	call		FINDSUM

   ldw		r4, 0(sp)					/* restore the value of r4 after the recursion */
	add		r2, r4, r2					/* return (N + FINDSUM(N-1)), which corresponds to
													return (r4 + r2) */
												
   ldw		ra, 4(sp)					/* restore return address for coming back from recursion */
	addi		sp, sp, 8					/* free space on the stack */
	ret
	
	.data
DATA_ADDR:
	.word		5 							
	.end
